extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)
#const MAX_FALL_SPEED = 600

var jump_count = 0
var max_jump_count = 2

var sprite_node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var velocity = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	sprite_node = get_node("Sprite")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func get_input():
    velocity.x = 0
    if (is_on_floor()):
        jump_count = 0
    if jump_count < max_jump_count and Input.is_action_just_pressed('up'):
        jump_count = jump_count + 1
        velocity.y = jump_speed
    if Input.is_action_pressed('right'):
        velocity.x += speed
        sprite_node.set_flip_h(false)
    if Input.is_action_pressed('left'):
        velocity.x -= speed
        sprite_node.set_flip_h(true)
    #if Input.is_action_pressed('up'):
    #   velocity.y -= speed
    #if Input.is_action_pressed('down'):
    #    velocity.y += speed

func _physics_process(delta):
    velocity.y += delta * GRAVITY
    get_input()
    velocity = move_and_slide(velocity, UP)